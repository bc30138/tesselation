from copy import deepcopy
import matplotlib.path as mpl_path

from tesselation.figure import Figure
from tesselation.utils import line_intersection, segment_intersect, plot_mutiple_figures


def parallelogram_grid(work_surface: Figure, figure):
    tmp_figure = deepcopy(figure)
    left_top_point = tmp_figure.left_top_point
    right_top_point = tmp_figure.right_top_point
    right_bottom_point = tmp_figure.right_bottom_point
    left_bottom_point = tmp_figure.left_bottom_point

    x_shift: float
    y_shift: float

    tmp_figure.calc_bounds()
    right_direction = False
    x_shift = work_surface.bounds['left'] - tmp_figure.bounds['left']
    if left_top_point[1] > right_top_point[1]:
        right_direction = True
        x_shift = work_surface.bounds['left'] - tmp_figure.bounds['left']
    elif left_top_point[1] < right_top_point[1]:
        x_shift = work_surface.bounds['right'] - tmp_figure.bounds['right']
    else:
        if left_top_point[0] <= left_bottom_point[0]:
            right_direction = True
            x_shift = work_surface.bounds['left'] - tmp_figure.bounds['left']
        else:
            x_shift = work_surface.bounds['right'] - tmp_figure.bounds['right']
    y_shift = work_surface.bounds['top'] - tmp_figure.bounds['top']

    left_top_point = (left_top_point[0] + x_shift,
                    left_top_point[1] + y_shift)
    right_top_point = (right_top_point[0] + x_shift,
                        right_top_point[1] + y_shift)
    right_bottom_point = (right_bottom_point[0] + x_shift,
                    right_bottom_point[1] + y_shift)
    left_bottom_point = (left_bottom_point[0] + x_shift,
                        left_bottom_point[1] + y_shift)


    if right_direction:
        intersection_x = line_intersection(
            (left_top_point, right_top_point),
            ((work_surface.bounds['right'], 0), (work_surface.bounds['right'], 1))
        )
        y_shift = work_surface.bounds['top'] - intersection_x[1]

        intersection_y = line_intersection(
            (left_top_point, left_bottom_point),
            ((0, work_surface.bounds['bottom']), (1, work_surface.bounds['bottom']))
        )
        x_shift = work_surface.bounds['left'] - intersection_y[0]

        top_line = (
            (right_top_point[0], right_top_point[1] + y_shift),
            (left_top_point[0], left_top_point[1] + y_shift)
        )
        left_line = (
            (left_bottom_point[0] + x_shift, left_bottom_point[1]),
            (left_top_point[0] + x_shift, left_top_point[1])
        )
        init_position = line_intersection(top_line, left_line)

        x_shift = init_position[0] - tmp_figure.left_top_point[0]
        y_shift = init_position[1] - tmp_figure.left_top_point[1]
    else:
        intersection_x = line_intersection(
            (left_top_point, right_top_point),
            ((work_surface.bounds['left'], 0), (work_surface.bounds['left'], 1))
        )
        y_shift = work_surface.bounds['top'] - intersection_x[1]

        intersection_y = line_intersection(
            (right_top_point, right_bottom_point),
            ((0, work_surface.bounds['bottom']), (1, work_surface.bounds['bottom']))
        )
        x_shift = work_surface.bounds['right'] - intersection_y[0]

        top_line = (
            (left_top_point[0], left_top_point[1] + y_shift),
            (right_top_point[0], right_top_point[1] + y_shift)
        )
        right_line = (
            (right_bottom_point[0] + x_shift, right_bottom_point[1]),
            (right_top_point[0] + x_shift, right_top_point[1])
        )
        init_position = line_intersection(top_line, right_line)

        x_shift = init_position[0] - tmp_figure.right_top_point[0]
        y_shift = init_position[1] - tmp_figure.right_top_point[1]


    for it, value in enumerate(tmp_figure.vertices):
        tmp_figure.vertices[it] = (value[0] + x_shift, value[1] + y_shift)
    tmp_figure.calc_bounds()

    move_x = (
        (tmp_figure.right_top_point[0] - tmp_figure.left_top_point[0]),
        (tmp_figure.right_top_point[1] - tmp_figure.left_top_point[1])
    )
    move_y = (
        (tmp_figure.left_bottom_point[0] - tmp_figure.left_top_point[0]),
        (tmp_figure.left_bottom_point[1] - tmp_figure.left_top_point[1])
    )
    stop_factor_column: float
    stop_move_column = None
    stop_factor_row = tmp_figure.bounds['top']
    if right_direction:
        init_stop_factor_column = tmp_figure.bounds['left']
        stop_move_column = lambda stop_factor_column: stop_factor_column > work_surface.bounds['right']
    else:
        move_x = (-move_x[0], -move_x[1])
        init_stop_factor_column = tmp_figure.bounds['right']
        stop_move_column = lambda stop_factor_column: stop_factor_column < work_surface.bounds['left']

    result_figures = []
    row = 0
    while stop_factor_row > work_surface.bounds['bottom']:
        column = 0
        stop_factor_column = init_stop_factor_column
        while not stop_move_column(stop_factor_column):
            next_figure = []
            for vertex in tmp_figure.vertices:
                next_figure.append(
                    (vertex[0] + column * move_x[0] + row * move_y[0],
                     vertex[1] + column * move_x[1] + row * move_y[1])
                )
            result_figures.append(next_figure)
            stop_factor_column += move_x[0]
            column += 1
        stop_factor_row += move_y[1]
        row += 1

    return result_figures


def polygon_contains(work_surface, figures, is_poly):
    mpl_surface = mpl_path.Path(work_surface.vertices)
    if is_poly:
        figure_n_vert = 3
    else:
        figure_n_vert = 2

    trim_result = []
    for result_figure in figures:
        if all(mpl_surface.contains_points(result_figure)):
            intersect = False
            it_poly = len(work_surface.vertices) - 1
            while it_poly > -1 and not intersect:
                it_figure = figure_n_vert
                while it_figure > -1:
                    intersect = segment_intersect(
                        work_surface.vertices[it_poly], work_surface.vertices[it_poly - 1],
                        result_figure[it_figure], result_figure[it_figure - 1]
                    )
                    it_figure -=1
                it_poly -= 1
            if not intersect:
                trim_result.append(result_figure)

    return trim_result


def parallelogram_tessellation(work_surface: Figure, figure: Figure):
    figure.order_points()
    result_figures = parallelogram_grid(work_surface, figure)
    trim_result = polygon_contains(work_surface, result_figures, True)
    n_figures = len(trim_result)
    area_prop = figure.area * n_figures / work_surface.area

    return trim_result, len(trim_result), area_prop


def triangle_tessellation(work_surface: Figure, figure: Figure):
    result_figures: list
    n_figures: int
    area_prop = float("-inf")
    for it in range(2, -1, -1):
        parallelogram = figure.vertices.copy()
        parallelogram.append(
            (
                parallelogram[it][0] +
                    (parallelogram[it - 1][0] - parallelogram[it - 2][0]),
                parallelogram[it][1] +
                    (parallelogram[it - 1][1] - parallelogram[it - 2][1])
            )
        )
        parallelogram_figure = Figure(parallelogram)
        parallelogram_figure.order_points()
        init_index = parallelogram_figure.vertices.index(parallelogram[it])
        grid_figures =\
            parallelogram_grid(work_surface, parallelogram_figure)
        triangle_figures = []
        for grid_figure in grid_figures:
            triangle_figures.append([
                grid_figure[init_index],
                grid_figure[init_index - 1],
                grid_figure[init_index - 3]
            ])
            triangle_figures.append([
                grid_figure[init_index - 2],
                grid_figure[init_index - 1],
                grid_figure[init_index - 3]
            ])
        loop_result_figures = []
        loop_result_figures = polygon_contains(work_surface, triangle_figures, False)
        loop_n_figures = len(loop_result_figures)
        loop_area_prop = figure.area * loop_n_figures / work_surface.area

        if loop_area_prop > area_prop:
            result_figures = loop_result_figures
            n_figures = loop_n_figures
            area_prop = loop_area_prop

    return result_figures, n_figures, area_prop


def hex_tessellation(work_surface: Figure, figure: Figure):
    pass