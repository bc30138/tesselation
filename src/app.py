import matplotlib

from tessellation import tessellation


def test_func():
    surfaces = [
        [(0, 0), (10, 0), (8, 10), (8, 4), (4, 5.5)],
        [(0, 0), (20, 0), (16, 20), (16, 8), (8, 11)],
        [(0, 0), (0, 6), (7, 6), (7, 0)],
        [(50, 9), (43, 22), (106, 124), (17, 36), (25, 33),
         (128, 75), (50, 66), (58, 87), (26, 17), (87, 123)]
    ]

    parallelograms = [
        [(0, 1), (0, 2), (1, 1), (1, 0)],
        [(0, 0), (0, 1), (1, 2), (1, 1)],
        [(0, 3), (0, 4), (1, 1), (1, 0)],
        [(2, 4), (3, 4), (6, 3), (7, 3)],
        [(0, 0), (0, 3), (1, 3), (1, 0)],
        [(0, 0), (1, 2), (3, 3), (2, 1)],
        [(1, 1), (0, 3), (2, 2), (3, 0)],
        [(0, 0), (1, 0), (1, 1), (0, 1)],
        [(-1, 2), (-1, -1), (1, 3), (1, 6)]
    ]

    triangles = [
        [(0, 1), (0, 2), (1, 1)],
        [(-1, 2), (-1, -1), (1, 3)]
    ]

    hexograms = [
        [(0, 0), (0, 1), (1, 2), (2, 2), (2, 1), (1, 0)],
        [(0, 1), (0, 2), (1, 2), (2, 1), (2, 0), (1, 0)],
        [(0, 1), (0, 2), (1, 3), (2, 2), (2, 1), (1, 0)]
    ]

    result_figures, n_figures, area_prop = tessellation(
        surfaces[1], triangles[0], 'data/result.png')

    print("N figures: " + str(n_figures))
    print("Filled area proportion: " + str(area_prop))
    print("Figures:")
    for it in range(0, 5):
        print(result_figures[it])
    print("...")
    for it in range(n_figures - 5, n_figures):
        print(result_figures[it])


if __name__ == "__main__":
    test_func()
